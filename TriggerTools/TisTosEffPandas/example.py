from root_pandas import read_root
from copy import deepcopy
from TisTos import *

branches = [
    # for cleaning the sample
    "B_DTF_PVandJpsi_M_flat",
    # for binning B phase space
    "B_PZ",
    "B_PT",
    # trigger lines we want the efficiency for
    "eplus_L0ElectronDecision_TOS",
    "eminus_L0ElectronDecision_TOS",
    # tigger lines we use to get the number of 
    # events before the trigger (TisTos technique)
    # Be careful, L0Global is prescaled!
    "B_L0Global_TOS",
    "B_L0Global_TIS",
    # our data-mc-weight, for data this can 
    # be the sweight
    "gb_weight_double"

]


file_name = (
    "/afs/cern.ch/work/t/ttekampe/public/TisTos/"
    "TisTos_example_data.root"
)


data = read_root(
    file_name,
    columns=branches
)

# clean up the data, for example by cutting on the Bmass with JPsi constraint
m_B = 5279
clean_cut = "B_DTF_PVandJpsi_M_flat>{}&B_DTF_PVandJpsi_M_flat<{}".format(m_B-80, m_B+80)
data_clean = data.query(clean_cut)


# events after our trigger selection
# if using weights, calculate the sum of weights instead
N_trig = len(data_clean.query("eplus_L0ElectronDecision_TOS|eminus_L0ElectronDecision_TOS"))
N_trig_w = data_clean.query("eplus_L0ElectronDecision_TOS|eminus_L0ElectronDecision_TOS")["gb_weight_double"].sum()
# data we use for TisTos
data_TIS = data_clean.query("B_L0Global_TIS")
data_TOS = data_clean.query("B_L0Global_TOS")
data_TISTOS = data_clean.query("B_L0Global_TIS&B_L0Global_TOS")

# optimize binning on TisTos data set, as this is the smallest compared to Tis or Tos
# Here we use 7 bins in B_PT and 7 bins in B_PZ
TisTos_binning = getBinning2D(
    data_TISTOS,
    "B_PT",
    7,
    "B_PZ",
    7
)

# now make copies for Tis and Tos
Tis_binning = deepcopy(TisTos_binning)
Tos_binning = deepcopy(TisTos_binning)

# fill the binnings with the corresponding data
# the default is to just count the events, 
# but it is also possible to additionally count
# the sum of weights for each bin, you just need
# to pass the name of the column in the data frame
# that contains the weights
fillBinning2D(Tis_binning, data_TIS, weightName="gb_weight_double")
fillBinning2D(Tos_binning, data_TOS, weightName="gb_weight_double")
fillBinning2D(TisTos_binning, data_TISTOS, weightName="gb_weight_double")

# if the binnings were filled using weights,
# you can set the last argument to True, to
# use these weights for the efficiency calculation

# calculate once with weights
tis_tos_eff_w = calculateTisTosEff(
    N_trig_w,
    Tis_binning,
    Tos_binning,
    TisTos_binning,
    True
)

# and once without weights
tis_tos_eff = calculateTisTosEff(
    N_trig,
    Tis_binning,
    Tos_binning,
    TisTos_binning,
    False
)

print("Efficiency with weights:")
print(tis_tos_eff_w)
print("\nEfficiency without weights:")
print(tis_tos_eff)