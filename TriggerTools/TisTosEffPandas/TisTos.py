import numpy as np
import math


class bin2D():
    '''
    Class representing a bin in a 2D histogram
    '''
    def __init__(self, varNameX, lowerX, upperX, varNameY, lowerY, upperY):
        self.varNameX = varNameX
        self.lowerX = lowerX
        self.upperX = upperX
        self.varNameY = varNameY
        self.lowerY = lowerY
        self.upperY = upperY
        self.nEntries = 0.
        self.value = 0.


def getBinning1D(varArray, nBins):
    '''
    Constructs a 1D binning with equal number of entries per bin

    Keyword arguments:
        varArray: array containing the values of the dimension to
            calculate the binning for
        nBins: number of bins

        returns a list containing the bin borders
    '''

    # copy the array before editing it
    varArray = np.array(varArray)
    varArray.sort()
    nEntriesPerBin = int(len(varArray)/nBins)
    binArray = [varArray[0]]

    for i in range(nBins - 1):
        binArray.append(varArray[(i+1)*nEntriesPerBin])

    binArray.append(varArray[-1])

    return binArray


def getBinning2D(df, varname1, nBins1, varName2, nBins2):
    '''
    Constructs a 2D binning with equal number of entries per bin

    Keyword arguments:
        df: pandas DataFrame containing the data
        varname1: name of the collumn of the 1st dimension of the binning
        nBins1: number of bins in the 1st dimension
        varname2: name of the collumn of the 2nd dimension of the binning
        nBins2: number of bins in the 2nd dimension

        returns a list of bin2D
    '''

    binningVar1 = getBinning1D(df[varname1].tolist(), nBins1)
    binningVar2 = []

    binning = []
    for i in range(len(binningVar1)-1):
        # print((varname1 + ">{0}&" + varname1 + "<{1}").format(binningVar1[i], binningVar1[i+1]))
        binningVar2 = getBinning1D(
            df.query((varname1 + ">{0}&" + varname1 + "<{1}").format(
                binningVar1[i],
                binningVar1[i+1]))[varName2].tolist(),
            nBins2
        )
        binning.append([])
        for j in range(len(binningVar2)-1):
            # print("i = {0}\tj = {1}".format(i, j))
            binning[i].append(
                bin2D(
                    varname1,
                    binningVar1[i],
                    binningVar1[i+1],
                    varName2,
                    binningVar2[j],
                    binningVar2[j+1]
                )
            )

    return binning


def printBinning(binning2D):
    for binning in binning2D:
        for bin in binning:
            print("x ranges from {0} to {1}".format(bin.lowerX, bin.upperX))
            print("y ranges from {0} to {1}".format(bin.lowerY, bin.upperY))
            print()


def fillBinning2D(binning2D, data, cut="", weightName=""):
    '''
    Fills a 2D binning with

    Keyword arguments:
        binning2D: predifined binning to fill
        data: pandas DataFrame containing the data
        cut: a cut data has to pass in order to be filled into the binning2D
        weightName: sWeight or data-MC-weight can also be ""
    '''
    _cut = cut if cut.startswith("&") or cut is "" else "&" + cut

    for binning1D in binning2D:
        for bin in binning1D:
            data_reduced = data.query(
                "{0}>{1}&{0}<{2}&{3}>{4}&{3}<{5}{6}".format(
                    bin.varNameX,
                    bin.lowerX,
                    bin.upperX,
                    bin.varNameY,
                    bin.lowerY,
                    bin.upperY,
                    _cut
                )
            )
            # print("{0}>{1}&{0}<{2}&{3}>{4}&{3}<{5}&{6}".format(bin.varNameX, bin.lowerX, bin.upperX, bin.varNameY, bin.lowerY, bin.upperY, cut))
            bin.nEntries = len(data_reduced)
            if weightName == "":
                bin.value = len(data_reduced)
            else:
                bin.value = data_reduced[weightName].sum()


def compareBinBorders(bin1, bin2):
    return (
        bin1.lowerX == bin2.lowerX and
        bin1.upperX == bin2.upperX and
        bin1.lowerY == bin2.lowerY and
        bin1.upperY == bin2.upperY
    )



def calculateTisTosEff(NtrigSel, tis2D, tos2D, tistos2D, useWeights=True):
    '''
    Calculates the TisTos efficiency

    Keyword arguments:
        NtrigSel: Number of events (sum of weights) after your trigger selection
        tis2D: 2DBinning filled with tis triggered data
        tos2D: 2DBinning filled with tos triggered data
        tistos2D: 2DBinning filled with data that fired tis and tos
        useWeights: use weights for the efficiency calculation

        returns (efficiency, uncertainty)
    '''

    denomOfEff = 0.
    sigmaNselSqr = 0.
    for tis1D, tos1D, tistos1D in zip(tis2D, tos2D, tistos2D):
        for tis, tos, tistos in zip(tis1D, tos1D, tistos1D):
            if not (compareBinBorders(tis, tos) and compareBinBorders(tis, tistos)):
                print(
                    "Can not calculate tistos variables because bin borders do not match!")
                raise
            else:
                if useWeights:
                    # same notation as in LHCb-INT-2013-038
                    N_TIS = tis.value
                    N_TOS = tos.value
                    N_TISTOS = tistos.value
                    b = tos.value - tistos.value
                    c = tis.value - tistos.value
                    d = tistos.value
                else:
                    N_TIS = tis.nEntries
                    N_TOS = tos.nEntries
                    N_TISTOS = tistos.nEntries
                    b = tos.nEntries - tistos.nEntries
                    c = tis.nEntries - tistos.nEntries
                    d = tistos.nEntries

                denomOfEff += N_TIS*N_TOS/N_TISTOS
                sigmaNselSqr += ((c-d)/d)**2 * b + \
                    (1-(b*c/(d**2)))**2 * c + ((b+d)/d)**2 * d

    eff = NtrigSel/denomOfEff
    n = NtrigSel
    m = NtrigSel/eff - NtrigSel

    sigmaSquareEtrig = (m / (n+m)**2)**2 * NtrigSel + \
        (-n/(n+m)**2)**2 * (sigmaNselSqr - NtrigSel)

    err = math.sqrt(sigmaSquareEtrig)
    
    return (eff, err)

