## Tis Tos notes

internal: https://cds.cern.ch/record/1557354/files/LHCb-INT-2013-038.pdf

public: https://cds.cern.ch/record/1701134/files/LHCb-PUB-2014-039.pdf

## Hints

* It does not matter which lines you actually TisTos, because the TisTos method 
only tells you, how many events you had before the trigger, here $`N`$.
The actual efficiency is then calculated as $`\varepsilon = N_{Trig}/N`$,
where $`N_{Trig}`$ is the number of events that fire the trigger you want the 
efficiency for.
$`N`$ is for example calculated as $`N=N_{Tis}/\varepsilon_{Tis}`$ with 
$`\varepsilon_{Tis} = N_{TisTos}/N_{Tos}`$.
* Be careful with prescaled trigger lines. Use the prescale emulator for MC.

## Documentation

This repository contains two implementations of the TisTosMethod. The original
one from the authors of the notes, and one that does not depend on root, but 
uses pandas.

### Pandas
For the methods using pandas see
[TisTosEffPandas/example.py](TisTosEffPandas/example.py) and the doc strings in 
the implementation in [TisTosEffPandas/TisTos.py](TisTosEffPandas/TisTos.py)
This implementation was tested on lxplus with `DaVinci/v42r7p2`.
For the example to work, you need to manually install root_pandas.
For example like this:
```
lb-run DaVinci/v42r7p2 bash
pip install root_pandas --user
```